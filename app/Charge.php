<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Charge extends Model 
{
    protected $table = 'charge';
    protected $fillable = ['trip_id', 'user_id', 'amount', 'cart', 'details', 'notification_code', 'payment_link', 'method_id', 'status_id', 'transaction_id', 'transaction_feedback', 'fulfilled_at', 'canceled_at', 'created_at', 'updated_at', 'cc' ];
    public $dates = ['start_at', 'end_at', 'created_at', 'updated_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public function status(){
        return $this->belongsTo('App\ChargeStatus', 'status_id');
    }

    public function trip(){
        return $this->belongsTo('App\Trip');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function method(){
        return $this->belongsTo('App\ChargeMethod');
    }

    public static function tableName(){
        $c = new Charge();
        return $c->getTable();
    }
}
