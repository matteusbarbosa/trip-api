<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ChargeMethod extends Model 
{
    protected $table = 'charge_method';
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'slug', 'details'];
}
