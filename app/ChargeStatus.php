<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ChargeStatus extends Model 
{
    protected $table = 'charge_status';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'slug', 'details'];
}
