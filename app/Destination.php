<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Destination extends Model 
{
    protected $table = 'destination';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'title', 'user_id', 'details', 'disabled_at', 'created_at', 'updated_at'
    ];

}
