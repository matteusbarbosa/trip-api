<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = ['user_id', 'mime', 'name', 'url', 'created_at', 'updated_at'];
    protected $table = 'file';
 

    public function getFillable(){
        return $this->fillable;
    }

}