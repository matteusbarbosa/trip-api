<?php

/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

/**
* Description of Visual
*
* @author cpd
*/

namespace App;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Config;
use App\Profile;

class Visual {
  /*
  * Entregue um período hoje - 7 dias
  */

  public static function datesub($days) {
    $date = date_create(date('Y-m-d'));
    date_sub($date, date_interval_create_from_date_string($days + " days"));
    return date_format($date, "Y-m-d");
  }

  /*
  * Entregue um período hoje + 7 dias
  */

  public static function dateadd($days) {
    $date = date_create(date('Y-m-d'));
    date_add($date, date_interval_create_from_date_string($days + " days"));
    return date_format($date, "Y-m-d");
  }

  /*
  *
  */

  public static function getDayName($n){
    $days = [0, 'sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];

    return $days[$n];
  }

  /*
  *
  */

  public static function showBreadcrumb($param = null, $withlink = null) {

    $path = Request::path();
    $current = explode('/', $path);
    $segments_count = count($current);
    $dom = [];
    $url = '';

    foreach ($current as $key => $name) {

      $name_display = $name == '' ? trans('legend.page-main') : $name;

      if($url == '/' . $name_display)
      continue;

      $url.= '/' . $name_display;

      $withlink = $key < $segments_count - 2;

      if (strlen($name) < config('constants.STRING_SIZE') ) {
       // $index_add = is_numeric($name) ? $key-1 : $key;
       
        $dom[$key] = ($withlink) ? '<a href="' . $url . '" class="text-uppercase">' . $name_display . '</a>' : '<span class="text-uppercase">' . $name_display . '</span>';
      }

      
    }

    if ($param == 'active') {
      return end($dom);
    } else {
      return $dom;
    }
  }

  final public static function dateYmd($date) {
    if($date == null)
    return null;
    return date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $date)));
  }

  final public static function dateDmy($date) {
    if($date == null)
    return null;

    return date('d/m/Y H:i', strtotime(str_replace('-', '/', $date)));
  }

  /**
  * @param dom : html de entrada da aplicação
  * @param format : formato do arquivo de saída
  * @param name : nome do arquivo de saída
  */
  final public static function replaceAccents($text) {

    $table = array(
      'Š' => 'S', 'š' => 's', 'Đ' => 'Dj', 'đ' => 'dj', 'Ž' => 'Z', 'ž' => 'z', 'Č' => 'C', 'č' => 'c', 'Ć' => 'C', 'ć' => 'c',
      'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E',
      'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O',
      'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss',
      'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e',
      'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o',
      'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'ý' => 'y', 'þ' => 'b',
      'ÿ' => 'y', 'Ŕ' => 'R', 'ŕ' => 'r',
    );
    return strtr($text, $table);
  }

  /**
  * @param $data formato BR ou EUA
  */
  final public static function dateToDays($date_start, $date_end) {

    $date_start = Visual::dateYmd($date_start);
    $date_end = Visual::dateYmd($date_end);

    $time_start = strtotime($date_start);
    $time_end = strtotime($date_end);

    $difference = $time_end - $time_start;

    $days = (int) floor($difference / (60 * 60 * 24));

    return $days;
  }

  /**
  * @param $data formato BR ou EUA
  */
  final public static function urlFriendly($string) {

    $modified = str_replace(' ', '-', strtolower(self::replaceAccents($string)));

    return $modified;
  }

  /*
  *
  *
  */

  public static function profilePicture($registration_id) {

    $userimage = Profile::where('registration_id', $registration_id)->pluck('picture');

    if (File::exists(config('constants.DIR_USERPIC') . $userimage)) {
      echo '<figure>'
      . '<img src="' . asset('app/' . config('constants.DIR_USERPIC') . $userimage) . '" class="img-responsive user-picture-thumb img-rounded">'
      . '<figcaption></figcaption>'
      . '</figure>';
    } else {
      echo '<img src="' . asset('app/profile_default.jpg') . '" class="img-responsive user-picture-thumb">';
    }
  }

  /*
  * Entregue a string p/ amaran js notify
  */

  public static function flash($icon, $title, $tag, $args = []) {
    return '<div class="notify-icon-wrapper"><i class="icon-' . $icon . '"></i></div><div class="notify-message-wrapper"><h3>' . trans('cognition.' . $title) . '</h3><p>' . trans('notify.' . $tag, $args) . '</p></div>';
  }

}
