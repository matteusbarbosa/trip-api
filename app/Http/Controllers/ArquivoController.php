<?php

namespace App\Http\Controllers;

use App\Arquivo;
use Illuminate\Http\Request;

class ArquivoController extends Controller
{
    public function show($id)
    {
        $arquivo = Arquivo::find($id);
        return response()->json($arquivo, 200);
    }

    public function store(Request $request)
    {
        $arquivo = self::persist(null, $request);
        $arquivo->save();
        return response()->json($arquivo, 200);
    }

    public function update($id, Request $request)
    {
        $arquivo = self::persist($id, $request);
        return response()->json($arquivo, 200);
    }

    private static function persist($id = null, Request $request){
        if($id == null){
            $arquivo = new Arquivo();
        } else {
            $arquivo = Arquivo::findOrFail($id);

            if(!empty($arquivo->arquivo_id))
            $arquivo->arquivo->delete();
        } 

        $arquivo = new Arquivo();
        $arquivo->nome = $request->input('nome');
        $arquivo->conteudo_blob = $request->input('conteudo_blob');
        $arquivo->mime = $request->input('mime');
        $arquivo->save();
    
        return $arquivo;        
    }

    public function destroy($id)
    {
        Arquivo::findOrFail($id)->delete();
        return response($id, 200);
    }
}