<?php
namespace App\Http\Controllers;
use App\Charge;
use App\User;
use PagSeguro; //Utilize a Facade
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
class ChargeController extends Controller
{
    public function index(Request $request)
    {
        $data = Charge::orderBy('id', 'DESC')
        ->take(config('settings.DB_FETCH_LIMIT'));
        //$operands = explode(',', config('settings.OPERANDS'));
        // $filters = collect($request->query())->reject(function($value, $key) use ($operands) { return in_array($key, $operands); });
        $whereMode = $this->getWhereMode($request->query('whereMode'));
        $queryFilters = $this->queryFiltered($request->query());      
        foreach($queryFilters as $k => $v){
            $whereGlue = $this->getWhereGlue($v);
            $queryTerm = $this->decodeQueryTerm($v);
            $data = $data->$whereMode($k, $whereGlue, $queryTerm);
        }
        if($request->query('selectMode') == 'pluck'){
            $list = $data->pluck($request->query('pluckValue'), $request->query('pluckKey'))->all();
            natcasesort($list);
            return response()->json($list, 200);
        }
        else
        return response()->json($this->mapResult($data->get()), 200);
    }
    private function mapResult($registers){
        return $registers->map(function($r){
            if($r->trip){
                $r->trip_name = $r->trip->title;
                $r->start_at_format = $r->trip->start_at->format('d/m/Y H:i');
                $r->end_at_format = $r->trip->end_at->format('d/m/Y H:i');
            }
            $r->status = $r->status;
            if(!empty($r->status)){
                $r->status_name = $r->status->title;
            }
            $r->user_name = $r->user->name;
            $r->method_name = $r->method->title;
            if(!empty($r->created_at)){
                $r->created_at_format = $r->created_at->format('d/m/Y H:i');
            }       
            return $r;
        });
    }
    public function checkingCopy($id)
    {
        $Charge = Charge::find($id);
        if(empty($Charge))
        throw new \Exception('Usuário não encontrado');
        $Charge->trip = $Charge->trip;
        $Charge->user = $Charge->user;
        $Charge->status = $Charge->status;
        $Charge->method = $Charge->method;
        $Charge->created_at_format = $Charge->created_at->format('d/m/Y H:i');
        return response()->json($Charge, 200);
    }
    public function store(Request $request)
    {
        $Charge = self::persist($request, null);
        return response()->json($Charge, 200);
    }
    public function update(Request $request, $id)
    {
        $Charge = self::persist($request, $id);
        return response()->json($Charge, 200);
    }
    public function contract(Request $request, $id){
        $Charge = Charge::findOrFail($id);
        $title = config('app.name')." – Reserva #".$Charge->id;
        $content = $Charge->contract;        
        Mail::send('emails.charge_contract', ['title' => $title, 'content' => $content], function ($message) use ($title)
        {
            $message->from(config('mail.from.address'), config('mail.from.name'));
            $message->to('matteusbarbosa2@gmail.com')->subject($title);
        });
    }
    public function getContract(){
        return \file_get_contents('contract.html');
    }
    private static function persist(Request $request, $id = null){
        $id = $request->input('id') != null ?  $request->input('id') : $id;
        if($id == null){
            $Charge = new Charge();
            if($request->input('user_id') !== null){
                $user = User::find($request->input('user_id'));
            } else {
                $user =  UserController::persist( $request, null);
            }
            $Charge->user_id = $user->id;
        } else {
            $Charge = Charge::findOrFail($id);
            $user = $Charge->user;
        }      
        $fields = $request->only($Charge->getFillable());
        $Charge->fill($fields);
        $Charge->save();
        $Charge->user = $Charge->user;
        return $Charge;
    }
    public function destroy($id)
    {
        Charge::findOrFail($id)->delete();
        return response('OK', 200);
    }
    public function request(Request $request){
        if(!empty($request->input('user_id'))){
            $user = UserController::persist($request, $request->input('user_id'));
        } else {
            $user = UserController::persist($request, null);
        }
        $cc = Crypt::encrypt([
            'cc_name' => $request->input('cc_name'),
            'cc_cpf' => $request->input('cc_cpf'),
            'cc_birthdate' => $request->input('cc_birthdate'),
            'cc_ddd' => $request->input('cc_ddd'),
            'cc_phone' =>$request->input('cc_phone'),
            'cc_street' => $request->input('cc_street'),
            'cc_number' => $request->input('cc_number'),
            'cc_complement' => $request->input('cc_complement'),
            'cc_district' => $request->input('cc_district'),
            'cc_postalcode' => $request->input('cc_postalcode'),
            'cc_addresscity' => $request->input('cc_city'),
            'cc_state' => $request->input('cc_state'),
            'cc_currency' => $request->input('cc_currency'),
            'cc_country' => $request->input('cc_country')
            ]);
            if(!empty($request->input('id'))){
                $charge = Charge::find($request->input('id'));
            } else {
                $charge = new Charge();
                $charge->user_id = $user->id;
                $charge->trip_id = $request->input('trip_id');
                $charge->method_id = $request->input('method_id');
                $charge->status_id = 5;
                $charge->details = $request->input('cc_details');
                $charge->cc = $cc;
                $charge->save();
            }
            return $charge;
        }
    }