<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    const SPECIAL = [
      'api_token', 'empresa', 'empresa_id', 'selectMode', 'whereMode', 'pluckKey', 'pluckValue', 'start_at', 'end_at'
    ];

    protected function queryFiltered($query){
        $queryFilters = collect($query)->reject(function($v, $k){
            return in_array($k, self::SPECIAL);
        });

        return $queryFilters;
    }

    protected function getWhereMode($whereMode = null){
        if($whereMode == null || $whereMode == 'and')
        return 'where';

        if($whereMode == 'or')
        return 'orWhere';

        else throw new \Exception('whereMode inválido.');
    }
    protected function getWhereGlue($term){
        if(substr($term, 0,1) == config('settings.DB_LIKE_OPEN') && substr( $term,-1) == config('settings.DB_LIKE_CLOSE')){
            return 'LIKE';
        }

        return '=';
    }
    protected function decodeQueryTerm($term){
   
        if(substr($term, 0,1) == config('settings.DB_LIKE_OPEN') && substr( $term,-1) == config('settings.DB_LIKE_CLOSE')){
           return '%'.substr($term, 1,-1).'%';
        }    
        
        if(substr($term, 0,1) == config('settings.DB_LIKE_OPEN')){
            return '%'.substr( $term,1);
        }    
        
        if(substr( $term,-1) == config('settings.DB_LIKE_CLOSE')){
            return substr( $term,0,-1).'%';
        }

        if($term == config('settings.DB_LIKE_OPEN').config('settings.DB_LIKE_CLOSE'))
        return '';

        return $term;
    }
}
