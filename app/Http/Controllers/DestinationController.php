<?php

namespace App\Http\Controllers;

use App\Destination;
use Illuminate\Http\Request;

class DestinationController extends Controller
{

    public function index(Request $request)
    {
        $data = Destination::orderBy('id', 'DESC')
        ->take(config('settings.DB_FETCH_LIMIT'));

        //$operands = explode(',', config('settings.OPERANDS'));

       // $filters = collect($request->query())->reject(function($value, $key) use ($operands) { return in_array($key, $operands); });
       $whereMode = $this->getWhereMode($request->query('whereMode'));
    
       $queryFilters = $this->queryFiltered($request->query());      

       if(!empty($request->query('empresa'))) {
           $data = $data->where('empresa', $request->query('empresa'));
       }


       foreach($queryFilters as $k => $v){
           $whereGlue = $this->getWhereGlue($v);
           $queryTerm = $this->decodeQueryTerm($v);
           $data = $data->$whereMode($k, $whereGlue, $queryTerm);
        }
        if($request->query('selectMode') == 'pluck'){
            $list = $data->pluck($request->query('pluckValue'), $request->query('pluckKey'))->all();
            natcasesort($list);
            return response()->json($list, 200);
        }
        else
        return response()->json($data->get(), 200);
    }

    public function show($id)
    {
        $Destination = Destination::find($id);

        if(empty($Destination))
        throw new \Exception('Usuário não encontrado');

        $Destination->created_at_format = $Destination->created_at->format('d/m/Y H:i');

        
        return response()->json($Destination, 200);
    }

    public function create(Request $request)
    {
        $Destination = Destination::create($request->all());
        return response()->json($Destination, 201);
    }

    public function store(Request $request)
    {
        $Destination = self::persist(null, $request);
        $Destination->save();

        return response()->json($Destination, 200);
    }

    public function update($id, Request $request)
    {
        $Destination = self::persist($id, $request);
        return response()->json($Destination, 200);
    }

    private static function persist($id = null, Request $request){

        if($id == null){
            $Destination = new Destination();
        } else {
            $Destination = Destination::findOrFail($id);
        }

        $fields = $request->only($Destination->getFillable());
        $Destination->fill($fields);
      
        $Destination->save();

        return $Destination;
    }

    public function destroy($id)
    {
        Destination::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}