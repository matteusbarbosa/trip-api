<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Crypt;
use App\EmpresaDetalhe;
use App\User;
use App\Responsavel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\LembreteSenha;

class LoginController extends Controller
{
    public function check(Request $request){

        $u = User::where('email', $request->input('email'))
        ->where('password', Crypt::encrypt($request->input('senha')))
        ->select('id')
        ->first();

        $status = !empty($u) ? 200 : 404;
        $success = !empty($u) ? true : false;

        return response()->json([
            'success' => $success,
            'status' => $status,
            'message' => ''
          ], $status);
    }

    public function sessionCheck(Request $request){

        $user = User::where('email', $request->query('email'))->first();
        $user->password_decode = Crypt::decrypt($user->password);

        //tokencheck já foi aplicado. Apenas retorne true
        return response()->json($user, 200);
    }

    

    public function show($id)
    {
        return response()->json(User::find($id));
    }

    public function create(Request $request)
    {
        $User = User::create($request->all());
        return response()->json($User, 201);
    }

    public function store(Request $request)
    {   
        $u = User::where('email', $request->input('email'))->first();

        if(empty($u))
        abort(404);

        $equal = Crypt::decrypt($u->password) == $request->input('password');

        $status =  $equal ? 200 : 404;
        $success = $equal ? true : false;

        return response()->json([
            'success' => $success,
            'status' => $status,
            'message' => ['role_id' => $u->role_id]
          ], $status);
    }

    //request login
    public function remind(Request $request){
        
        $data = [];
        $usuario = User::where('email', $request->input('email'))->first();
 
        Mail::to($usuario->email)
        ->send(new LembreteSenha($usuario));
        
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => '',
            'email' => $usuario->empresa_data->detalhe->email
          ], 200);

    }



}