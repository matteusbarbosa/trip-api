<?php

namespace App\Http\Controllers;

use App\EmpresaDetalhe;
use App\User;
use App\Responsavel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\LembreteSenha;
use Illuminate\Support\Facades\Crypt;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use App\Charge;

class PagSeguroController extends Controller     
{

    //primeiro passo
    public function startSession(Request $request){
        
        $Url="https://ws.pagseguro.uol.com.br/sessions?email=".config('pagseguro.email')."&token=".config('pagseguro.token');

        $client = new \GuzzleHttp\Client([
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'
            ],
            'base_uri' => config('app.url'),
            'verify' => false]);
    $res = $client->request('POST', $Url);

     $id = \simplexml_load_string($res->getBody())[0]->id;
     return response()->json($id, 200);
    }

    //último passo
    public function checkout(Request $request){
        
        $Url="https://ws.pagseguro.uol.com.br/v2/transactions?email=".config('pagseguro.email')."&token=".config('pagseguro.token');

        $client = new \GuzzleHttp\Client([
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'
            ],
            'base_uri' => config('app.url'),
            'verify' => false]);


            try {
                    
                $res = $client->request('POST', 
                $Url,
                [
                    'body' => $request->input('data')
                ]);
            
                $transaction = \simplexml_load_string($res->getBody());
            
                if(!empty($transaction->errors)){
                    return response()->json($transaction->errors, 200);
                }
            
                $Charge = Charge::find($transaction->reference);
                $Charge->payment_link = $transaction->paymentLink[0];
                $Charge->status_id = 1;
                $Charge->save();

                return response()->json($Charge, 200);

            } catch (ClientException $e) {
                return response()->json([ 'error' => true, 'message' => $e->getMessage()], 200);
            }

    }


    //notificações
    public function update(Request $request)
    {

                $psEmail = config('pagseguro.email');
                $psToken = config('pagseguro.token');

                //notificationTpype, notificationCode

                $url = 'https://ws.pagseguro.uol.com.br/v3/transactions/notifications/'.$_POST['notificationCode'].'?email='.$psEmail.'&token='.$psToken;
                $ch = curl_init($url);

                //curl_setopt($ch, CURLOPT_FILE, $myfile);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPGET, 1);
                curl_setopt($ch, CURLOPT_URL, $url );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
                curl_setopt($ch, CURLOPT_DNS_USE_GLOBAL_CACHE, false );
                curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 2 );

                $output = curl_exec($ch);

                //var_dump($output);
                $transaction = simplexml_load_string($output);

                $Charge = Charge::find($transaction->reference);
                $Charge->notification_code = $request->input('notification_code');
                $Charge->payment_link = $transaction->paymentLink;
                $Charge->status_id = $transaction->status;
                $Charge->save();

                self::confirm($Charge->id);


                curl_close($ch);

            return response()->json(true,200);
    }

    public function confirm($id){

            
        $Charge = Charge::findOrFail($id);
        $user = $Charge->user;
       

    $email_list = $Charge->toArray();
    $email_list['Viagem'] = $Charge->trip->title;

    $content = "<ul>";

    $email_list['Status'] = __('pagseguro.status_'.$Charge->status_id);

    foreach($email_list as $k => $v){
        if(!is_array($v))
        $content .= '<li>'.__('mail.charge_'.$k).': '.$v.'</li>'; 
    }

    $content .= "</ul>";


    $title = config('app.name')." – Reserva #".$id;
    $email = $user->email;

    Mail::send('emails.charge_confirm', ['title' => $title, 'content' => $content], function ($message) use ($title, $email)
    {

        $message->from(config('mail.from.address'), config('mail.from.name'));

        $message->to($email)->subject($title);
        $message->to('matteusbarbosa2@gmail.com')->subject($title);

    });
}


}