<?php

namespace App\Http\Controllers;

use App\Trip;
use App\File;
use Illuminate\Http\Request;

class TripController extends Controller
{

    protected function queryFiltered($query){
        $reject = self::SPECIAL;
        $reject[] = 'id';
        $queryFilters = collect($query)->reject(function($v, $k) use ($reject){
            return in_array($k, $reject);
        });

        return $queryFilters;
    }

    public function index(Request $request)
    {
        $data = Trip::orderBy('id', 'DESC')
        ->take(config('settings.DB_FETCH_LIMIT'));

        //$operands = explode(',', config('settings.OPERANDS'));

       // $filters = collect($request->query())->reject(function($value, $key) use ($operands) { return in_array($key, $operands); });
       $whereMode = $this->getWhereMode($request->query('whereMode'));
    
       $queryFilters = $this->queryFiltered($request->query());      


       if(!empty($request->query('id'))) {
        $data = $data->where(Trip::tableName().'.id', $request->query('id'));
        }

       if(!empty($request->query('start_at'))) {
        $data = $data->whereDate(Trip::tableName().'.start_at', '>=', $request->query('start_at'));
        }

        if(!empty($request->query('end_at'))) {
            $data = $data->whereDate(Trip::tableName().'.end_at', '<=', $request->query('end_at'));
        }

        $data = Trip::filterAvailable($data);


       foreach($queryFilters as $k => $v){
           $whereGlue = $this->getWhereGlue($v);
           $queryTerm = $this->decodeQueryTerm($v);
           $data = $data->$whereMode($k, $whereGlue, $queryTerm);
        }
        if($request->query('selectMode') == 'pluck'){
            $list = $data->pluck($request->query('pluckValue'), $request->query('pluckKey'))->all();
            natcasesort($list);
            return response()->json($list, 200);
        }
        else
        return response()->json($this->mapResult($data->get()), 200);
    }

    private function mapResult($registers){

        return $registers->map(function($r){
            
            if(!empty($r->start_at)){
                $r->start_at_format = $r->start_at->format('d/m/Y H:i');
            }
               
                if(!empty($r->end_at)){
                    $r->end_at_format = $r->end_at->format('d/m/Y H:i');
                }

                  
                if(!empty($r->disabled_at)){
                    $r->disabled_at_format = $r->disabled_at->format('d/m/Y H:i');
                } else {
                    $r->disabled_at_format = 'Ativa';
                }     
                
                $r->charge_count = $r->charge->where('status_id', 3)->count();

                if(!empty($r->file_id)){
                    $r->file = $r->file;
                }
            return $r;

        });
    }

    public function show($id)
    {
        $Trip = Trip::find($id);

        if(empty($Trip))
        throw new \Exception('Usuário não encontrado');

        $Trip->user = $Trip->user;
        $Trip->created_at_format = $Trip->created_at->format('d/m/Y H:i');
        $Trip->charge_count = $Trip->charge()->where('status_id', 2)->count();
        $Trip->charge_sum = $Trip->charge_count * $Trip->amount;

                    
        if(!empty($Trip->start_at)){
            $Trip->start_at_format = $Trip->start_at->format('d/m/Y H:i');
        }
           
            if(!empty($Trip->end_at)){
                $Trip->end_at_format = $Trip->end_at->format('d/m/Y H:i');
            }

    

        if(!empty($Trip->file_id)){
            $Trip->file = $Trip->file;
        }
        
        return response()->json($Trip, 200);
    }

    public function store(Request $request)
    {
        $Trip = self::persist($request);
        return response()->json($Trip, 200);
    }

    public function update(Request $request, $id)
    {
        
        $Trip = self::persist($request, $id);
        return response()->json($Trip, 200);
    }

    private static function saveFile(Request $request, $trip){

        if($request->file('file') === null){
            return null;
        }

        
        $file = new File();

        if(isset($trip->file_id) && $trip->file_id !== null){
            $file = $trip->file;
        }

        $fileName = uniqid().'_'.$request->file('file')->getClientOriginalName();
        $destinationPath = 'uploads';

        $file->name = $fileName;
        $file->user_id = $request->input('user_id');
        $file->url = config('app.url').'/'.$destinationPath.'/'.$fileName;
        $file->mime = $request->file('file')->getClientMimeType();
        $file->save();
        
        $trip->file_id = $file->id;
        $trip->save();

        try{
            $request->file('file')->move($destinationPath, $fileName);
        } catch(FileException $e){

        }
    }

    private static function persist( Request $request, $id = null){

        if($id === null){
            $Trip = new Trip();
        } else {
            $Trip = Trip::findOrFail($id);
        }

        $fields = $request->only($Trip->getFillable());
     
        $Trip->fill($fields);

        
      
        $Trip->save();

        self::saveFile($request, $Trip);

        return $Trip;  
    }

    public function destroy($id)
    {
        Trip::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }

    public function destroyFile($id)
    {
        $trip = Trip::findOrFail($id);

        $File = File::find($trip->file_id);
        $File->delete();
        $trip->file_id = null;
        $trip->save();
        
        return response($id, 200);
    }
}