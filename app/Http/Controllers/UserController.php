<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class UserController extends Controller
{

    public function index(Request $request)
    {
        $data = User::orderBy('id', 'DESC')
        ->take(config('settings.DB_FETCH_LIMIT'));

        //$operands = explode(',', config('settings.OPERANDS'));

       // $filters = collect($request->query())->reject(function($value, $key) use ($operands) { return in_array($key, $operands); });
       $whereMode = $this->getWhereMode($request->query('whereMode'));
    
       $queryFilters = $this->queryFiltered($request->query());      

       if(!empty($request->query('empresa'))) {
           $data = $data->where('empresa', $request->query('empresa'));
       }

       foreach($queryFilters as $k => $v){
           $whereGlue = $this->getWhereGlue($v);
           $queryTerm = $this->decodeQueryTerm($v);
           $data = $data->$whereMode($k, $whereGlue, $queryTerm);
        }
        if($request->query('selectMode') == 'pluck'){
            $list = $data->pluck($request->query('pluckValue'), $request->query('pluckKey'))->all();
            natcasesort($list);
            return response()->json($list, 200);
        }
        else
        return response()->json($data->get(), 200);
    }

    public function show($id)
    {
        $User = User::find($id);

        if(empty($User))
        throw new \Exception('Usuário não encontrado');
        
        return response()->json($User, 200);
    }

    public function create(Request $request)
    {
        $User = User::create($request->all());
        return response()->json($User, 201);
    }

    public function store(Request $request)
    {
        $User = self::persist($request, null);
        $User->save();

        return response()->json($User, 200);
    }

    public function update(Request $request, $id = null )
    {
        $User = self::persist($request, $id);
        return response()->json($User, 200);
    }

    public static function persist(Request $request, $id = null){

        if($id == null){
            $User = new User();
        } else {
            $User = User::findOrFail($id);
        }

        $fields = $request->only($User->getFillable());
        $fields['password'] = Crypt::encrypt(substr($fields['phone'], -4));
        $User->fill($fields);
      
        $User->save();

        return $User;
    }

    public function destroy($id)
    {
        User::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}