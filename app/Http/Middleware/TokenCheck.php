<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class TokenCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = null;

        if(empty($request->header('Authorization')))
        return response('Não autorizado.', 401);

        if ($request->header('Authorization')) {
            $key = str_replace("Bearer ", "", $request->header('Authorization'));
            
            if($key == 'null')
            return response()
            ->json([
              'success' => false,
              'status' => 403,
              'message' => 'HTTP_FORBIDDEN'
            ], 403);
            

            $split = explode('||', base64_decode($key));
            $user = User::where('email', $split[0])
            ->where('password', $split[1])
            ->first();
        }



        return $next($request);
    }
}
