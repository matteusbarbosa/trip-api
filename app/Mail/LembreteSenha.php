<?php

namespace App\Mail;

use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LembreteSenha extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Order
     */
    protected $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * Build the message.
     *
     * @return $this
    */
    public function build()
    {
        $data = [];
        $data['emil'] = $this->usuario->email;
        $data['password'] = $this->usuario->password;

        return $this->subject(config('app.name'))
        ->view('emails.lembrete_senha')->with('data', $data);
    }
}