<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Trip extends Model 
{
    protected $table = 'trip';
    public $dates = ['start_at', 'end_at', 'disabled_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'title','destination_id', 'user_id', 'contract', 'details', 'disabled_at', 'amount_table', 'discount_percent', 'charge_max', 'start_at', 'end_at', 'created_at', 'updated_at'
    ];

    public function getFillable(){
        return $this->fillable;
    }

   public static function tableName(){
        $c = new Trip();
        return $c->getTable();
    }
    public function charge(){
        return $this->hasMany('App\Charge');
    }

    public static function filterAvailable($sentence){
        return $sentence
        ->selectRaw(Trip::tableName().'.*, count('.Charge::tableName().'.id) AS '.Charge::tableName().'_count')
        ->leftJoin(Charge::tableName(), function ($join) {
            $join->on( Trip::tableName().'.id', '=', Charge::tableName().'.trip_id')
            ->where(Charge::tableName().'.status_id', 3);
        })
        ->withCount(Charge::tableName())
        ->whereNull(Trip::tableName().'.disabled_at')
        ->groupBy(Trip::tableName().'.id');
    }
    public function file(){
        return $this->belongsTo('App\File');
    }
}
