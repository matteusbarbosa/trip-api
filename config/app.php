<?php

return [
    'url' => env('APP_URL'),
    'name' => env('APP_NAME'),
    'key' => env('APP_KEY','eLCqSkvtfo5PHJz343fEcLkuOzQfTlT4'),
    'cipher' => env('APP_CIPHER','AES-256-CBC'),
    'locale' => 'pt-BR'
];