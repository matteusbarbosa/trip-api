<?php

return [
    'DB_DASHBOARD_FETCH_LIMIT' => env('DB_DASHBOARD_FETCH_LIMIT', 5),
    'DB_FETCH_LIMIT' => env('DB_FETCH_LIMIT', 200),
    'DB_REPORT_FETCH_LIMIT' => env('DB_REPORT_FETCH_LIMIT', 200),
    'DB_LIKE_OPEN' => '{',
    'DB_LIKE_CLOSE' => '}'
];