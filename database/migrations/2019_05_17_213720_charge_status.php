<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChargeStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charge_status', function($table){
            $table->increments('id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->text('details')->nullable(true);
          });

          $status = [
            'Aguardando pagamento' => 'O comprador iniciou a transação, mas até o momento o PagSeguro não recebeu nenhuma informação sobre o pagamento.',
            'Em análise' => 'O comprador optou por pagar com um cartão de crédito e o PagSeguro está analisando o risco da transação.',
            'Paga' => 'A transação foi paga pelo comprador e o PagSeguro já recebeu uma confirmação da instituição financeira responsável pelo processamento.',
            'Disponível' => 'A transação foi paga e chegou ao final de seu prazo de liberação sem ter sido retornada e sem que haja nenhuma disputa aberta.',
            'Em disputa' => 'O comprador, dentro do prazo de liberação da transação, abriu uma disputa.',
            'Devolvida' => 'O valor da transação foi devolvido para o comprador.',
            'Cancelada' => 'A transação foi cancelada sem ter sido finalizada.'
          ];
    
          foreach($status as $title => $description){
            DB::table('charge_status')->insert([
              'title' => $title,
              'slug' => \App\Visual::urlFriendly($title),
              'details' => $description
          ]);
          }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('charge_status');
    }
}
