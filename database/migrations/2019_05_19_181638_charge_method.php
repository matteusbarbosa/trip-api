<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChargeMethod extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charge_method', function($table){
            $table->increments('id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->text('details')->nullable(true);
            $table->timestamps();
          });
      
      $methods = [
        'Crédito Online com PagSeguro' => 'Pagar online com PagSeguro',
        'Boleto Online com PagSeguro' => 'Pagar online com PagSeguro'
      ];
        
      
      foreach($methods as $title => $description){
        DB::table('charge_method')->insert([
          'title' => $title,
          'slug' => \App\Visual::urlFriendly($title),
          'details' => $description
      ]);
      }


     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('charge_method');
    }
}
