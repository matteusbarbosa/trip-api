<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class User extends Migration
{
 /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->tinyInteger('role_id')->default(1);
            $table->string('surname')->nullable(true);
            $table->enum('gender', ['M', 'W'])->nullable(true);
            $table->string('email')->nullable(true);
            $table->string('cpf')->nullable(true);
            $table->char('ddd', 3)->nullable(true);
            $table->string('phone')->nullable(true);
            $table->text('password', 32);
            $table->text('details')->nullable(true);
            $table->timestamp('last_active_at')->nullable(true);
            $table->timestamp('disabled_at')->nullable(true);
            $table->timestamps();
            
            $table->index('role_id');
        });

    

        Schema::table('user', function (Blueprint $table) { 

       /*      
            $table->foreign('role_id')
            ->references('id')->on('role')
            ->onDelete('cascade');
        */

        });

      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
