<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Destination extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('destination', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->unsignedBigInteger('user_id');
            $table->text('details')->nullable(true);
            $table->timestamp('disabled_at')->nullable(true);
            
            $table->timestamps();
            
            $table->index('user_id');
        });

        Schema::table('destination', function (Blueprint $table) { 

            
            $table->foreign('user_id')
            ->references('id')->on('user')
            ->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('destination');
    }
}
