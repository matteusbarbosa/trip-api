<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class File extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('name')->nullable();
            $table->string('mime')->nullable();
            $table->string('url')->nullable();

            
            $table->timestamps();
            
            $table->index('user_id');
        });

        Schema::table('file', function (Blueprint $table) { 



            
           $table->foreign('user_id')
            ->references('id')->on('user')
            ->onDelete('cascade');
 

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file');
    }
}
