<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Trip extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->unsignedBigInteger('destination_id')->nullable();
            $table->unsignedBigInteger('file_id')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->text('details')->nullable(true);
            $table->text('contract')->nullable(true);
            $table->timestamp('disabled_at')->nullable(true);
            $table->json('amount_table')->nullable(true);
            $table->integer('discount_percent')->default(0);
            $table->integer('charge_min')->nullable(true);
            $table->integer('charge_max')->nullable(true);
            $table->timestamp('start_at')->nullable(true);
            $table->timestamp('end_at')->nullable(true);
            
            $table->timestamps();
            
            $table->index('user_id');
        });

        Schema::table('trip', function (Blueprint $table) { 


            $table->foreign('destination_id')
            ->references('id')->on('destination')
            ->onDelete('cascade');

            $table->foreign('file_id')
            ->references('id')->on('file')
            ->onDelete('cascade');
            
           $table->foreign('user_id')
            ->references('id')->on('user')
            ->onDelete('cascade');
 

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip');
    }
}
