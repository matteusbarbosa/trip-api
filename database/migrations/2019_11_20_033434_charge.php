<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Charge extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charge', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('trip_id')->nullable(true);
            $table->unsignedBigInteger('user_id');
            $table->double('amount',10,2);
            $table->json('cart')->nullable(true);
            $table->text('details')->nullable(true);
            $table->string('notification_code')->nullable(true);
            $table->text('payment_link')->nullable(true);
            $table->integer('method_id');
            $table->integer('status_id');
            $table->string('transaction_id')->unique()->nullable();
            $table->string('transaction_feedback')->nullable();
            $table->timestamp('fulfilled_at')->nullable(true);
            $table->timestamp('canceled_at')->nullable(true);
            $table->timestamps();            
            $table->text('cc')->nullable(true);    

            $table->index('trip_id');
            $table->index('user_id');
        });

        Schema::table('charge', function (Blueprint $table) { 

            
            $table->foreign('user_id')
            ->references('id')->on('user')
            ->onDelete('cascade');


            $table->foreign('trip_id')
            ->references('id')->on('trip')
            ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charge');
    }
}
