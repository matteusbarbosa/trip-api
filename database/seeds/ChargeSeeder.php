<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;

class ChargeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $faker = Faker\Factory::create();
      for($c = 0; $c < 20; $c++){
   
          $cc = Crypt::encrypt([
              'name' => $faker->name,
              'cc_cpf' => $faker->randomElement(['118.950.086-89', '115.587.458-96', '454.667.558-78']),
              'cc_birthdate' => $faker->randomElement(['01/01/2000', '10/10/2005', '05/05/1990']),
              'cc_ddd' => 31,
              'cc_phone' =>$faker->randomElement(['998745865', '985784565', '985235458']),
              'cc_street' => $faker->name,
              'cc_number' => rand(1,85),
              'cc_complement' => $faker->name,
              'cc_district' => $faker->name,
              'cc_postalcode' => $faker->randomElement(['32235-150', '25487-152']),
              'cc_addresscity' => $faker->city,
              'cc_state' => $faker->randomElement(['SP','MG', 'RJ']),
              'cc_currency' => 'BRL',
              'cc_country' => 'BRA'
          ]
          );

          $i_id = DB::table('charge')->insertGetId([
              'user_id' => rand(1,20),
              'trip_id' => rand(1,20),
              'details' => $faker->realText,
              'amount' => $faker->randomElement([ 100.10, 250.50, 300.50, 800.50, 1566.50]),
              'cart' => '{"age": 0, "boarding": 1, "room": 2}',
              'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
              'updated_at' => $faker->dateTimeBetween('-5 years', 'now'),
              'transaction_id' => $faker->randomNumber().$c,
              'transaction_feedback' => $faker->randomElement(['Test feedback', null]),
              'method_id' => rand(1,2),
              'status_id' => rand(1,5),
              'fulfilled_at' => $faker->randomElement([date('Y-m-d H:i:s'), null]),
              'canceled_at' => $faker->randomElement([date('Y-m-d H:i:s'), null]),
              'cc' => $cc   
          ]);           
      }


    }
}
