<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call('UserSeeder');
         $this->call('DestinationSeeder');
         $this->call('TripSeeder');
         $this->call('ChargeSeeder');
    }
}
