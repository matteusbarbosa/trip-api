<?php

use Illuminate\Database\Seeder;
use App\Item;
use App\Stock;
use Illuminate\Support\Facades\DB;

class DestinationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($c = 0; $c < 20; $c++){
     
            $i_id = DB::table('destination')->insertGetId([
                'user_id' => rand(1,20),
                'title' => $faker->name,
                'details' => $faker->realText,
                'disabled_at' => $faker->randomElement([null, $faker->dateTimeBetween('-5 years', 'now')]),
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now'),
        
            ]);

           
        }
    }

}
