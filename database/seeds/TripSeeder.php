<?php

use Illuminate\Database\Seeder;
use App\Item;
use App\Stock;
use Illuminate\Support\Facades\DB;

class TripSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {



        $at = [
            'age' => (object) [
              (object)  [
                    'name' => 'de 0 a 5 anos',
                    'min' => 0,
                    'max' => 5,
                    'amount' => 0
                ],
                (object)  [
                    'name' => 'de 6 a 10 anos',
                    'min' => 6,
                    'max' => 10,
                    'amount' => 320
                ],
                (object)    [
                    'name' => 'Adultos, a partir de 11 anos',
                    'min' => 11,
                    'amount' => 100
                ]
            ],
            'room' => [
                (object)  [
                    'name' => 'Triplo',
                    'amount' => 100
                ],
                (object)   [
                    'name' => 'Duplo',
                    'amount' => 150
                ]
            ],
            'boarding' => (object) [
                (object)   [
                    'name' => 'Praça estação',
                    'date' => '2019-12-31 23:59',
                    'amount' => 10
                ],
                (object) [
                    'name' => 'Praça da liberdade',
                    'date' => '2019-12-31 22:59',
                    'amount' => 15
                ],
            ]
            ];

        $faker = Faker\Factory::create();
        for($c = 0; $c < 20; $c++){
     
            $i_id = DB::table('trip')->insertGetId([
                'user_id' => rand(1,20),
                'title' => $faker->name,
                'details' => $faker->realText,
                'disabled_at' => $faker->randomElement([null, $faker->dateTimeBetween('-5 years', 'now')]),
                'destination_id' => rand(1,20),
                'amount_table' => json_encode($at),
                'discount_percent' =>  $faker->randomElement([0, 15]),
                'charge_min' =>  $faker->randomElement([0, 25]),
                'charge_max' =>  $faker->randomElement([25, 48]),
                'start_at' => $faker->dateTimeBetween('+1 years', '+2 years'),
                'end_at' => $faker->dateTimeBetween('now', '+5 years'),
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
        
            ]);

           
        }
    }

}
