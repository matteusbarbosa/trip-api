<?php

use Illuminate\Database\Seeder;
use App\Item;
use App\Stock;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($c = 0; $c < 20; $c++){
     
            $i_id = DB::table('user')->insertGetId([
                'name' => $faker->name,
                'role_id' => $faker->randomElement([1,10]),
                'surname' => $faker->name,
                'email' => $faker->email,
                'phone' => $faker->randomElement(['998745865', '985784565', '985235458']),
                'ddd' => $faker->randomElement(['31', '21', '41']),
                'gender' => $faker->randomElement(['M', 'W']),
                'password' => Crypt::encrypt(10),
                'details' => $faker->realText,
                'disabled_at' => $faker->randomElement([null, $faker->dateTimeBetween('-5 years', 'now')]),
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
            ]);

           
        }
    }

}
