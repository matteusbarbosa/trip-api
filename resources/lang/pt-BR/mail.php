<?php

return [
    'ticket_id' => 'ID de reserva',
    'ticket_user_id' => 'ID de usuário',
    'ticket_trip_event_id' => 'ID de viagem',
    'ticket_method_id' => 'ID de método de pagamento',
    'ticket_status_id' => 'ID de status de pagamento',
    'ticket_created_at' => 'Data de cadastro',
    'ticket_updated_at' => 'Data de atualização',
    'ticket_details' => 'Detalhes',
    'ticket_payment_link' => 'Link do pagamento',
    'ticket_transaction_id' => 'ID da transação',
    'ticket_fulfilled_at' => 'Data da conclusão',
    'ticket_canceled_at' => 'Data do cancelamento'    
];