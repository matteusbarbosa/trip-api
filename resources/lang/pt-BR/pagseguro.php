<?php

return [
    'status_1' => 'Aguardando pagamento. O comprador iniciou a transação, mas até o momento o PagSeguro não recebeu nenhuma informação sobre o pagamento.',
    'status_2' => 'Em análise. O comprador optou por pagar com um cartão de crédito e o PagSeguro está analisando o risco da transação.',
    'status_3' => 'Paga.A transação foi paga pelo comprador e o PagSeguro já recebeu uma confirmação da instituição financeira responsável pelo processamento.',
    'status_4' => 'Disponível. A transação foi paga e chegou ao final de seu prazo de liberação sem ter sido retornada e sem que haja nenhuma disputa aberta.',
    'status_5' => 'Em disputa. O comprador, dentro do prazo de liberação da transação, abriu uma disputa.',
    'status_6' => 'Devolvida. O valor da transação foi devolvido para o comprador.',
    'status_7' => 'Cancelada. A transação foi cancelada sem ter sido finalizada.'
];