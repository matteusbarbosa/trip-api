<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Crypt;

function resource($router, $uri, $controller){


    $router->get($uri, $controller.'@index');
    $router->get($uri.'/create', $controller.'@create');
    $router->post($uri, $controller.'@store');
    $router->get($uri.'/{id}', $controller.'@show');
    $router->get($uri.'/{id}/edit', $controller.'@edit');
    $router->put($uri.'/{id}', $controller.'@update');
    $router->patch($uri.'/{id}', $controller.'@update');
    $router->delete($uri.'/{id}', $controller.'@destroy');
   
    return $router;
}

$router->get('/', function () use ($router) {
    return Crypt::encrypt(10);
    return $router->app->version();
});


//$mid = env('APP_ENV') == 'production' ? 'token-check' : null;
$mid = 'token-check';
$router->group(['middleware' => $mid], function () use ($router) {
    $router->get('session-check', 'LoginController@sessionCheck');
$router = resource($router, 'user', 'UserController');
$router->put('charge-contract/{id}', 'ChargeController@contract');
$router->put('charge-confirm/{id}', 'ChargeController@confirm');
$router->get('charge', 'ChargeController@index');


$router->delete('trip/{id}/delete-file', 'TripController@destroyFile');
$router->delete('trip/{id}', 'TripController@destroy');


$router = resource($router, 'destination', 'DestinationController');

});

$router = resource($router, 'login', 'LoginController');
$router->get('get-contract', 'ChargeController@getContract');
$router->post('charge', 'ChargeController@store');
$router->put('charge', 'ChargeController@update');
$router->get('charge/{id}', 'ChargeController@checkingCopy');
$router->get('pagseguro-start-session', 'PagSeguroController@startSession');
$router->post('pagseguro-update', 'PagSeguroController@update');
$router->post('pagseguro-checkout', 'PagSeguroController@checkout');
$router->get('trip', 'TripController@index');
$router->put('trip/{id}', 'TripController@update');
$router->post('trip', 'TripController@store');
$router->get('trip/{id}', 'TripController@show');
